"""
Representation of all query predicates
"""
from abc import ABC, abstractmethod
from typing import List, Union

Number = Union[int, float]
Schema = List[dict]
Fields = Union[Number, str]


class Predicate(ABC):
    """Base predicate class"""

    def __init__(self, schema: Schema):
        self._schema = schema
        self._acceptable_field_types = []  # this must be override
        self._str_field_types = []  # this must be override
        self._operators = []  # this must be override

    @abstractmethod
    def get_sql_chunk(self, field: str, value: any, operator: str) -> str:
        """
        :param field: the field to select in a table.
        :param value: the value used to filter the result.
        :param operator: the operator to use in the WHERE clause.
        :return: a string representing a portion of the sql request.
        """
        pass

    def _check_field_and_value(self, field: str, value: Fields) -> None:
        """
        Checks if field and value are correct and raises exceptions if it is not the case.
        :param field: the field to select in a table.
        :param value: the value used to filter the result.
        """
        if field not in self.fields:
            raise ValueError(f'{field} is not present in the database schema')

        if not isinstance(value, tuple(self._acceptable_field_types)):
            raise TypeError(
                f'the value of the field {field} must have one of the following types: {self._str_field_types}')

        if field in self.fields:
            field_type = self._get_field_type(field)
            if not isinstance(value, field_type):
                raise TypeError(f'the value of the field {field} must be of type {field_type}')

    def _check_operator(self, operator: str) -> None:
        """
        Checks if the operator is available for the predicate and raises exceptions if it is not the case.
        :param operator: the operator to use in the WHERE clause.
        """
        if operator not in self._operators:
            raise ValueError(f'the operator {operator} is not permitted for this type of field')

    @property
    def fields(self) -> List[str]:
        """
        :return: the list of all fields in a schema.
        """
        return [field['name'] for field in self._schema]

    @property
    def allowed_operators(self) -> List[str]:
        """
        :return: the list of all available operators.
        """
        return self._operators

    def _get_field_type(self, field: str) -> any:
        """
        :param field: the field to select in a table.
        :return: the type associated to the field.
        """
        for _field in self._schema:
            if _field['name'] == field:
                return _field['type']


class NumericalPredicate(Predicate):
    """Predicate which represents numeric fields"""

    def __init__(self, schema: Schema):
        super().__init__(schema)
        self._acceptable_field_types = [int, float]
        self._str_field_types = ['int', 'float']
        self._operators = ['gte', 'gt', 'lte', 'lt', 'eq']
        self._sql_operators = {'gte': '>=', 'gt': '>', 'lte': '<=', 'lt': '<', 'eq': '='}

    def get_sql_chunk(self, field: str, value: any, operator: str) -> str:
        self._check_field_and_value(field, value)
        self._check_operator(operator)
        return f'{field} {self._sql_operators[operator]} {value}'


class CategoricalPredicate(Predicate):
    """Class which represents categorical fields like string or boolean"""

    def __init__(self, schema: Schema):
        super().__init__(schema)
        self._acceptable_field_types = [str]
        self._str_field_types = ['str']
        self._operators = ['eq', 'contains']

    def get_sql_chunk(self, field: str, value: any, operator: str) -> str:
        self._check_field_and_value(field, value)
        self._check_operator(operator)
        if operator == 'eq':
            return f"{field} = '{value}'"
        if operator == 'contains':
            return f"{field} LIKE '%{value}%'"
        return ''
