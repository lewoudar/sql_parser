"""
A simple module to prove the reliability of the sql parser
"""
from query import Query


def print_query(query: dict) -> None:
    schema = [
        {"name": "url", "type": str},
        {"name": "http_code", "type": int},
        {"name": "title", "type": str},
        {"name": "delay", "type": float}
    ]
    query_object = Query(query, schema)

    print('query: ', query)
    print('sql: ', query_object.get_query())
    print('==========================')


if __name__ == '__main__':
    query = {'fields': ['url', 'http_code']}
    print_query(query)
    query = {
        'fields': ['url'],
        'filters': {
            'field': 'http_code',
            'value': 200
        }
    }
    print_query(query)
    query = {
        'fields': ['url'],
        'filters': {
            'field': 'http_code',
            'value': 200,
            'predicate': 'gt'
        }
    }
    print_query(query)
    query = {
        'fields': ['url'],
        'filters': {
            'and': [
                {
                    'field': 'http_code',
                    'value': 200,
                    'predicate': 'gt'
                },
                {
                    'field': 'title',
                    'value': 'Kevin',
                    'predicate': 'contains'
                }
            ]
        }
    }
    print_query(query)
    query = {
        'fields': ['url', 'title'],
        'filters': {
            'and': [
                {
                    'field': 'http_code',
                    'value': 200,
                    'predicate': 'eq',
                },
                {
                    'or': [
                        {
                            'field': 'title',
                            'value': 'kevinos',
                            'predicate': 'contains'
                        },
                        {
                            'field': 'url',
                            'value': 'https://kevin.com',
                            'predicate': 'eq'
                        }
                    ]
                }
            ]
        }
    }
    print_query(query)
