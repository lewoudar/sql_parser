"""
Module which aims to test predicate classes and especially the method get_sql_chunk
"""
import unittest
from predicates import NumericalPredicate, CategoricalPredicate


class PredicateTest(unittest.TestCase):
    """Class used to test predicate classes"""

    def setUp(self):
        self.schema = [
            {"name": "url", "type": str},
            {"name": "http_code", "type": int},
            {"name": "title", "type": str},
            {"name": "delay", "type": float}
        ]
        self.np = NumericalPredicate(self.schema)
        self.cp = CategoricalPredicate(self.schema)

    def test_correct_numeric_predicate(self):
        operators = {'gte': '>=', 'gt': '>', 'lte': '<=', 'lt': '<', 'eq': '='}
        for key, value in operators.items():
            result = self.np.get_sql_chunk('delay', 7.5, key)
            self.assertEqual(f'delay {value} 7.5', result)
        result = self.np.get_sql_chunk('http_code', 200, 'eq')
        self.assertEqual('http_code = 200', result)

    def test_bad_numeric_predicate(self):
        bad_field = 'foo'
        field = 'http_code'
        operator = 'contains'
        with self.assertRaises(ValueError) as e:
            self.np.get_sql_chunk(bad_field, 200, 'gt')
        self.assertEqual(f'{bad_field} is not present in the database schema', e.exception.args[0])

        with self.assertRaises(TypeError) as e:
            self.np.get_sql_chunk(field, 2.5, 'gt')
        self.assertIn(f'the value of the field {field} must be of type', e.exception.args[0])

        with self.assertRaises(ValueError) as e:
            self.np.get_sql_chunk(field, 200, operator)
        self.assertEqual(f'the operator {operator} is not permitted for this type of field', e.exception.args[0])

        with self.assertRaises(TypeError) as e:
            self.np.get_sql_chunk(field, 'foo', 'lt')
        self.assertIn(f'the value of the field {field} must have one of the following types:', e.exception.args[0])

    def test_correct_categorical_predicate(self):
        result = self.cp.get_sql_chunk('title', 'my title', 'eq')
        self.assertEqual("title = 'my title'", result)
        result = self.cp.get_sql_chunk('url', 'kevin.com', 'contains')
        self.assertEqual("url LIKE '%kevin.com%'", result)

    def test_bad_categorical_predicate(self):
        field = 'title'
        operator = 'gt'
        with self.assertRaises(TypeError) as e:
            self.cp.get_sql_chunk(field, 200, 'eq')
        self.assertIn(f'the value of the field {field} must have one of the following types:', e.exception.args[0])

        with self.assertRaises(ValueError) as e:
            self.cp.get_sql_chunk(field, 'str', operator)
        self.assertEqual(f'the operator {operator} is not permitted for this type of field', e.exception.args[0])
