"""
Module which aims to test query module.
"""
import unittest

from query import Query


class QueryTest(unittest.TestCase):
    """Class used to test query module"""

    def setUp(self):
        self.schema = [
            {"name": "url", "type": str},
            {"name": "http_code", "type": int},
            {"name": "title", "type": str},
            {"name": "delay", "type": float}
        ]
        self.base = 'crawls'

    def test_good_query_1(self):
        query = {'fields': ['url', 'http_code']}
        query_object = Query(query, self.schema)
        self.assertEqual(f'SELECT url, http_code FROM {self.base}', query_object.get_query())

    def test_good_query_2(self):
        query = {
            'fields': ['url'],
            'filters': {
                'field': 'http_code',
                'value': 200
            }
        }
        query_object = Query(query, self.schema)
        self.assertEqual(f'SELECT url FROM {self.base} WHERE http_code = 200', query_object.get_query())

    def test_good_query_3(self):
        query = {
            'fields': ['url'],
            'filters': {
                'field': 'http_code',
                'value': 200,
                'predicate': 'gt'
            }
        }
        query_object = Query(query, self.schema)
        self.assertEqual(f'SELECT url FROM {self.base} WHERE http_code > 200', query_object.get_query())

    def test_good_query_4(self):
        query = {
            'fields': ['url'],
            'filters': {
                'and': [
                    {
                        'field': 'http_code',
                        'value': 200,
                        'predicate': 'gt'
                    },
                    {
                        'field': 'title',
                        'value': 'Kevin',
                        'predicate': 'contains'
                    }
                ]
            }
        }
        query_object = Query(query, self.schema)
        result = query_object.get_query()
        self.assertEqual(f"SELECT url FROM {self.base} WHERE http_code > 200 AND title LIKE '%Kevin%'", result)

    def test_good_query_5(self):
        query = {
            'fields': ['url', 'title'],
            'filters': {
                'and': [
                    {
                        'field': 'http_code',
                        'value': 200,
                        'predicate': 'eq',
                    },
                    {
                        'or': [
                            {
                                'field': 'title',
                                'value': 'kevinos',
                                'predicate': 'contains'
                            },
                            {
                                'field': 'url',
                                'value': 'https://kevin.com',
                                'predicate': 'eq'
                            }
                        ]
                    }
                ]
            }
        }
        query_object = Query(query, self.schema)
        result = query_object.get_query()
        expected = f"SELECT url, title FROM {self.base} WHERE http_code = 200 AND "
        expected += f"( title LIKE '%kevinos%' OR url = 'https://kevin.com' )"
        self.assertEqual(expected, result)

    def _test_error(self, query: dict, error: type, message: str) -> None:
        query_object = Query(query, self.schema)
        with self.assertRaises(error) as e:
            query_object.get_query()
        self.assertEqual(message, e.exception.args[0])

    def test_fields_missing(self):
        query = {}
        self._test_error(query, KeyError, 'there must be a "fields" key in your request object')

    def test_bad_fields_key(self):
        query = {'fields': []}
        self._test_error(query, ValueError, '"fields" must have at least one entry')
        query = {'fields': {'hello'}}
        self._test_error(query, TypeError, '"fields" must be a list')
        bad_field = 'magenta'
        query = {'fields': ['url', bad_field]}
        self._test_error(query, ValueError, f'the field "{bad_field}" is not recognized in the schema')
        bad_field = 2.0
        query = {'fields': ['url', bad_field]}
        self._test_error(query, ValueError,
                         f'"fields" must have only string values, which is not the case of "{bad_field}"')
