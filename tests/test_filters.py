"""
Module which aims to test query filters
"""
import unittest
from filters import Filter


class FilterTest(unittest.TestCase):
    """Class used to test filters"""

    def setUp(self):
        self.schema = [
            {"name": "url", "type": str},
            {"name": "http_code", "type": int},
            {"name": "title", "type": str},
            {"name": "delay", "type": float}
        ]

    def test_filter_without_conditional_operator(self):
        filters = {
            'field': 'http_code',
            'value': 200,
            'predicate': 'gt'
        }
        my_filter = Filter(self.schema, filters)
        result = my_filter.get_filter_chunk()
        self.assertEqual(result, 'WHERE http_code > 200')

    def test_filter_with_conditional_1(self):
        filters = {
            'and': [
                {
                    'field': 'http_code',
                    'value': 200,
                    'predicate': 'gt'
                },
                {
                    'field': 'title',
                    'value': 'Kevin',
                    'predicate': 'contains'
                }
            ]
        }
        my_filter = Filter(self.schema, filters)
        result = my_filter.get_filter_chunk()
        self.assertEqual(result, "WHERE http_code > 200 AND title LIKE '%Kevin%'")

    def test_filter_with_conditional_2(self):
        filters = {
            'or': [
                {
                    'field': 'http_code',
                    'value': 200,
                    'predicate': 'gt'
                },
                {
                    'field': 'title',
                    'value': 'Kevin',
                    'predicate': 'contains'
                },
                {
                    'field': 'url',
                    'value': 'https://kevin.com',
                    'predicate': 'eq'
                }
            ]
        }
        my_filter = Filter(self.schema, filters)
        result = my_filter.get_filter_chunk()
        self.assertEqual(result, "WHERE http_code > 200 OR title LIKE '%Kevin%' OR url = 'https://kevin.com'")

    def test_filter_with_conditional_recursively(self):
        filters = {
            'and': [
                {
                    'field': 'http_code',
                    'value': 200,
                    'predicate': 'eq',
                },
                {
                    'or': [
                        {
                            'field': 'title',
                            'value': 'kevinos',
                            'predicate': 'contains'
                        },
                        {
                            'field': 'url',
                            'value': 'https://kevin.com',
                            'predicate': 'eq'
                        }
                    ]
                }
            ]
        }
        my_filter = Filter(self.schema, filters)
        result = my_filter.get_filter_chunk()
        self.assertEqual(result, "WHERE http_code = 200 AND ( title LIKE '%kevinos%' OR url = 'https://kevin.com' )")

    def test_empty_filters_object(self):
        filters = {}
        with self.assertRaises(ValueError) as e:
            my_filter = Filter(self.schema, filters)
            my_filter.get_filter_chunk()
        self.assertEqual(e.exception.args[0], 'your filters object is empty')

    def test_conditional_operator_list(self):
        filters = {
            'and': []
        }
        with self.assertRaises(ValueError) as e:
            my_filter = Filter(self.schema, filters)
            my_filter.get_filter_chunk()
        self.assertIn('must have at least two entries', e.exception.args[0])

        filters['and'] = [{}]
        with self.assertRaises(ValueError) as e:
            my_filter = Filter(self.schema, filters)
            my_filter.get_filter_chunk()
        self.assertIn('must have at least two entries', e.exception.args[0])

        filters['and'] = [{}, {}]
        with self.assertRaises(KeyError) as e:
            my_filter = Filter(self.schema, filters)
            my_filter.get_filter_chunk()
        self.assertIn('there must be a', e.exception.args[0])

    def test_bad_conditional_operator(self):
        filters = {
            'xor': []
        }
        with self.assertRaises(ValueError) as e:
            my_filter = Filter(self.schema, filters)
            my_filter.get_filter_chunk()
        self.assertEqual('"xor" is not a valid conditional operator', e.exception.args[0])
        filters = {
            'and': [
                {
                    'field': 'http_code',
                    'value': 200,
                    'predicate': 'eq',
                },
                {
                    'xor': []
                }
            ]
        }
        with self.assertRaises(ValueError) as e:
            my_filter = Filter(self.schema, filters)
            my_filter.get_filter_chunk()
        self.assertEqual('"xor" is not a valid conditional operator', e.exception.args[0])
