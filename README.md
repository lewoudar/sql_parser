# SQL PARSER

This is a little library to parse structured JSON object and transform them into SQL queries.
To run this program, you will need python3.6

The script sqlparser.py doesn't work yet. To test the program, you can run main.py.
You can edit main.py to add your own tests to check the reliability of the library.
