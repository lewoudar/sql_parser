"""
A script which permit to test the tiny dsl created.
"""
import argparse
import json
import sys

from query import Query

parser = argparse.ArgumentParser(prog='SQL Parser')
parser.add_argument('-s', help='a JSON representation of the schema')
parser.add_argument('-q', help='a JSON representation of the query')
args = parser.parse_args()
mapping = {'str': str, 'int': int, 'float': float}

if not args.s:
    sys.exit('you must provide a schema in order to transform a JSON query')
if not args.q:
    sys.exit("you don't provide a JSON object to parse")

try:
    schema = json.loads(args.s)
    query = json.loads(args.q)
except json.decoder.JSONDecodeError as e:
    sys.exit(e)

try:
    query_object = Query(query, schema)
    print(query_object.get_query())
except Exception as e:
    sys.exit(e)
