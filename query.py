"""
Module which aims to represent an SQL query
"""
from typing import List

from filters import Filter


class Query:
    """Class which represents an sql query"""

    def __init__(self, query: dict, schema: List[dict]):
        self._query = query
        self._table = 'crawls'
        self._schema = schema

    def _check_query(self) -> None:
        fields = self._query.get('fields')
        if fields is None:
            raise KeyError('there must be a "fields" key in your request object')
        if not isinstance(fields, list):
            raise TypeError('"fields" must be a list')
        if not fields:
            raise ValueError('"fields" must have at least one entry')
        schema_fields = [item['name'] for item in self._schema]
        for field in fields:
            if not isinstance(field, str):
                raise ValueError(f'"fields" must have only string values, which is not the case of "{field}"')
            if field not in schema_fields:
                raise ValueError(f'the field "{field}" is not recognized in the schema')

    def get_query(self) -> str:
        self._check_query()
        fields = self._query['fields']
        result = 'SELECT ' + ', '.join(fields) + f' FROM {self._table}'
        filters = self._query.get('filters', {})
        if filters:
            my_filter = Filter(self._schema, filters)
            return result + ' ' + my_filter.get_filter_chunk()
        return result
