"""
Module which aims to format query filters
"""
from typing import List

from predicates import NumericalPredicate, CategoricalPredicate


class Filter:
    """Class which represents a filter condition"""

    def __init__(self, schema: List[dict], fields: dict):
        self._allowed_conditions = {'and': 'AND', 'or': 'OR'}
        self._fields = fields
        self._numeric_predicate = NumericalPredicate(schema)
        self._categorical_predicate = CategoricalPredicate(schema)

    def _get_condition_filter_chunk(self, condition: str, fields: List[dict], deep=False) -> str:
        """
        :param condition: a conditional operator (and, or).
        :param fields: a list of fields used to combine a chunk of filter statement.
        :param deep: a boolean useful to add brackets if conditional operators are used recursively.
        :return: a string representing a chunk of filter statement.
        """
        if not isinstance(fields, list):
            raise TypeError(f'the value of "{condition}" condition must be a list')
        if not fields or len(fields) < 2:
            raise ValueError(f'the condition key "{condition}" must have at least two entries')
        chunks = [self._get_field_chunk(field, test_condition=True) for field in fields]
        pattern = f' {self._allowed_conditions[condition]} '.join(chunks)
        if deep:
            return f'( {pattern} )'
        return pattern

    def _get_field_chunk(self, field: dict, test_condition=False) -> str:
        """
        :param field: a filters sub-object.
        :param test_condition: boolean to know if we need to check the conditional operator in the field object.
        :return: a string representing a chunk of the filter statement related to field object.
        """
        other_chunks = ''
        if test_condition and self._conditional_operator_exists(field):
            for key, value in field.items():
                if key.lower() in self._allowed_conditions.keys():
                    other_chunks += self._get_condition_filter_chunk(key.lower(), value, deep=True)
        # if we only have conditional operator in our field object
        # we can stop the process here and return the filter chunk
        if self._only_conditional_operator(field):
            return other_chunks
        operator = field.get('predicate', 'eq')
        data_field = field.get('field', '')
        value = field.get('value')
        if not field:
            raise KeyError('there must be a "field" key in your filters sub-object')
        if value is None:
            raise KeyError('there must be a "value" key in your filters sub-object')
        if isinstance(value, str):
            return self._categorical_predicate.get_sql_chunk(data_field, value, operator)
        return self._numeric_predicate.get_sql_chunk(data_field, value, operator)

    def _only_conditional_operator(self, field_object: dict) -> bool:
        """
        :param field_object: an object representing a filters sub-object.
        :return: True if there is only conditional operator in the filter sub-object.
        """
        keys = list(field_object.keys())
        return len(keys) == 1 and keys[0].lower() in self._allowed_conditions.keys()

    def _check_bad_conditional_operator(self, fields: dict) -> None:
        """
        Checks if there is a bad conditional operator in the fields object and raises error if it is the cas.
        :param fields: an object representing the filters object or a sub-object.
        """
        for key, value in fields.items():
            if isinstance(value, list) and key.lower() not in self._allowed_conditions.keys():
                raise ValueError(f'"{key}" is not a valid conditional operator')

    def _conditional_operator_exists(self, fields: dict = None) -> bool:
        """
        :param fields: an object representing the filters object or a sub-object.
        :return: True if there is a conditional operator in the filters object keys, False if not.
        """
        fields = self._fields if fields is None else fields
        self._check_bad_conditional_operator(fields)
        field_keys = [key.lower() for key in fields]
        for condition in self._allowed_conditions.keys():
            if condition in field_keys:
                return True
        return False

    def get_filter_chunk(self) -> str:
        """
        :return: the filter statement.
        """
        if not self._fields:
            raise ValueError('your filters object is empty')
        self._check_bad_conditional_operator(self._fields)
        result = 'WHERE '
        if not self._conditional_operator_exists():
            return result + self._get_field_chunk(self._fields)

        for key, value in self._fields.items():
            if key.lower() in self._allowed_conditions.keys():
                result += self._get_condition_filter_chunk(key.lower(), value)
            else:
                raise ValueError(f'"{key}" is not a valid conditional operator')

        return result
